package model;

public class Card {
    private String id;
    private String status;
    private int pin;

    public Card(String id, String status, int pin) {
        this.id = id;
        this.status = status;
        this.pin = pin;
    }

    public int getPin() { return pin; }

    public void setPin(int pin) { this.pin = pin; }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public void activate() { this.status = "Active"; }

    public void deactivate() { this.status = "Inactive"; }


}
