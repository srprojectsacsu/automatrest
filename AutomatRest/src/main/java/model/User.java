package model;

public class User {
    private String firstName;
    private String lastName;
    private String email;
    private String username;
    private String password;
    private String pictureURL;
    private String accountType;
    private Card card;

    public User(String firstName, String lastName, String email, String username, String password, String pictureURL, String accountType) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.email = email;
        this.username = username;
        this.password = password;
        this.pictureURL = pictureURL;
        this.accountType = accountType;
    }
    public User(String username, String password) {
        this.firstName = "";
        this.lastName = "";
        this.email = "";
        this.username = username;
        this.password = password;
        this.pictureURL = "";
        this.accountType = "Regular";
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getPictureURL() {
        return pictureURL;
    }

    public void setPictureURL(String pictureURL) {
        this.pictureURL = pictureURL;
    }

    public String getAccountType() {
        return accountType;
    }

    public void setAccountType(String accountType) {
        this.accountType = accountType;
    }
}
