package controller;
import java.util.Hashtable;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import service.UserService;
import model.User;

@CrossOrigin(origins = "*", allowedHeaders = "*")
@RestController
public class UserController {

    @RequestMapping("/")
    public String index() {
        return "Homepage of GeldAutomat API";
    }

    @Autowired
    UserService us;

    @RequestMapping("/all")
    public List<User> getAll() {
        return us.getAll();
    }

    /*@RequestMapping("{accountNumber}")
    public User getPerson(@PathVariable("accountNumber") String accountNumber) {
        return us.getUser(accountNumber);
    }*/

}
