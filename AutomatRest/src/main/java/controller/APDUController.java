package controller;

import java.util.Hashtable;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import service.UserService;
import service.APDUService;
import model.Card;
import model.User;


@CrossOrigin(origins = "*", allowedHeaders = "*")
@RestController
@RequestMapping("/card")
public class APDUController {
    @Autowired
    APDUService apduS;

    @GetMapping("/verify")
    public String verification(@RequestParam("pin") int pin) {
        byte Wallet_CLA = (byte) 0xB0;
        byte VERIFY = (byte) 0x20;

        systemConnection();
        //verify
        byte lengthDataField = 4; //4 character passcode
        byte maxDataBytesExpected = 0;
        byte instParamtr1 = 0;
        byte instParamtr2 = 0;

        char[] ip = String.valueOf(pin).toCharArray();
        byte[] data_v = new byte[ip.length];

        byte[] cmnds_v = {Wallet_CLA, VERIFY, instParamtr1, instParamtr2};
        apduS.setTheAPDUCommands(cmnds_v);
        apduS.setTheDataLength(lengthDataField);

        for (int ipIndx = 0; ipIndx < ip.length; ipIndx++) {
            data_v[ipIndx] = (byte) (ip[ipIndx] & 0x0F);
        }
        apduS.setTheDataIn(data_v);
        apduS.setExpctdByteLength(maxDataBytesExpected);
        //setTheCommandAPDUOnGUI(cmnds_v, data_v, lengthDataField, maxDataBytesExpected);
        apduS.exchangeTheAPDUWithSimulator();
        apduS.decodeStatus();
        byte[] stat = apduS.decodeStatus();
        String caseCmnd = apduS.atrToHex(stat[0]) + apduS.atrToHex(stat[1]);
        systemClose();
        if (caseCmnd.matches("6312"))
        {
            return "Wrong pin" + pin;
        }
        return "Successful login with pin " + pin;
    }
    private void systemConnection() {
        // TODO add your handling code here:

        byte lengthDataField = 6;
        byte maxDataBytesExpected = 0;
        byte instParamtr1 = 0x04;
        byte instParamtr2 = 0;

        apduS.establishConnectionToSimulator();
        apduS.pwrUp();
        //select
        byte[] cmnds = {(byte) 0x00, (byte) 0xA4, instParamtr1, instParamtr2};
        apduS.setTheAPDUCommands(cmnds);
        apduS.setTheDataLength(lengthDataField);
        byte[] data = {(byte) 0x7C, (byte) 0x0F, (byte) 0x1B, (byte) 0x01, (byte) 0x88, (byte) 0x01};
        apduS.setTheDataIn(data);
        apduS.setExpctdByteLength(maxDataBytesExpected);
        //setTheCommandAPDUOnGUI(cmnds, data, lengthDataField, maxDataBytesExpected);
        apduS.exchangeTheAPDUWithSimulator();
        apduS.decodeStatus();
       //setTheResponseAPDUOnGUI(maxDataBytesExpected);

        System.out.println("Connection established to cardreader stimulator.");
    }
    //close the connection with the cardreader stimulator
    private void systemClose() {
        // TODO add your handling code here:
        apduS.pwrDown();
        apduS.closeConnection();
        System.out.println("Connection closed to cardreader stimulator.");
    }

}
