package service;

import java.util.List;
import java.util.ArrayList;
//import java.util.Hashtable;
import org.springframework.stereotype.Service;
import model.User;

@Service
public class UserService {
    List<User> users = new ArrayList<>();

    public UserService() {
        User p = new User("Paul", "password123");
        p.setFirstName("Paul");
        p.setLastName("Verbovshchuk");
        users.add(p);

        p = new User("Jlo", "p123");
        p.setFirstName("Jeff");
        p.setLastName("Low");
        users.add(p);
    }

    /*public User getUser(String pin) {
        if (users.contains(pin)) {
            int ind = indexOf(pin);
            return users.get(ind);
        }
        else
            return null;
        if (users.indexOf() != -1)
        {

        }
        else
            return null;
    }*/

    public List<User> getAll() {
        return users;
    }
}
